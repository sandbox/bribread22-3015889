core = 8.x
api = 2
defaults[projects][subdir] = contrib

projects[bootstrap][type] = theme
projects[bootstrap][version] = 3.15

projects[address][type] = module
projects[address][version] = 1.4
projects[commerce][type] = module
projects[commerce][version] = 2.11
projects[entity][type] = module
projects[entity][version] = 1.0-rc1
projects[inline_entity_form][type] = module
projects[inline_entity_form][version] = 1.0-rc1
